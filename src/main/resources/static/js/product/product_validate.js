$(document).ready(function() {

	$('#add-product-form').validate({
		errorElement: 'lable',
		errorClass: 'error',
		rules: {
			name: {
				required: true,
			},
			portfolioId: {
				required: true,
			},
			categoryId: {
				required: true,
			},
			supplierId: {
				required: true,
			},
			imgFile1: {
				required: true,
			},
			imgFile2: {
				required: true,
			},
			imgFile3: {
				required: true,
			},
			imgFile3: {
				required: true,
			},
			label: {
				required: true,
			},
			price: {
				required: true,
				number:   true
			},
			quantity: {
				required: true,
				number: true,
				min: 1
			},
			keyword: {
				required: true,
			},
		},

		// messages: {
		//     name: {
		//         required: 'msg'
		//     },
		//     email: {
		//         required: "msg"
		//     }
		// },
		errorClass: "error",
		validClass: "valid success-alert"
	});
	
	//handle validate from Server
	
	$('input').focus(function() {
		$('.error').text('');
	});
});