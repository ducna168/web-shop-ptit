package ptit.com.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import ptit.com.entities.Category;

public interface CategoryRepository extends JpaRepository<Category, Long>{
	public Boolean existsByName(String name);
	
	public Page<Category> findByNameContains( String name, Pageable pageable);
	
}
