package ptit.com.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import ptit.com.entities.Supplier;

public interface SupplierRepository extends JpaRepository<Supplier, Long>{
	public Boolean existsByName(String name);
	
	public Page<Supplier> findByNameContains( String name, Pageable pageable);
	
}
