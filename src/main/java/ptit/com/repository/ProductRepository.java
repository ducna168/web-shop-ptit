package ptit.com.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import ptit.com.entities.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{
	public Boolean existsByName(String name);
	
	public Page<Product> findByNameContains( String name, Pageable pageable);
}
