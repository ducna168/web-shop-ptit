package ptit.com.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import ptit.com.entities.Portfolio;

public interface PortfolioRepository extends JpaRepository<Portfolio, Long>{
	public Boolean existsByName(String name);
	
	public Page<Portfolio> findByNameContains( String name, Pageable pageable);
	
}
