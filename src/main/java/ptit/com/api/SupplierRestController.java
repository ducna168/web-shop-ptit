package ptit.com.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ptit.com.entities.Supplier;
import ptit.com.model.PageInfo;
import ptit.com.service.SupplierService;

@RestController
@RequestMapping("admin/supplier")
public class SupplierRestController {
	@Autowired 
	public SupplierService supplierService;
	
	@PostMapping("view")
	public Page<Supplier> getSupplierByPageInfo(@ModelAttribute("pageInfo") PageInfo pageInfo) {
		return supplierService.findByPageInfo(pageInfo);
	}
	
}
