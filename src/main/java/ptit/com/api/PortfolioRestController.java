package ptit.com.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ptit.com.entities.Portfolio;
import ptit.com.model.PageInfo;
import ptit.com.service.PortfolioService;

@RestController
@RequestMapping("admin/portfolio")
public class PortfolioRestController {
	@Autowired 
	public PortfolioService portfolioService;
	
	@PostMapping("view")
	public Page<Portfolio> getPortfolioByPageInfo(@ModelAttribute("pageInfo") PageInfo pageInfo) {
		System.out.println("toi day ne!!!");
		return portfolioService.findByPageInfo(pageInfo);
	}
	
}
