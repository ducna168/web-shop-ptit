package ptit.com.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ptit.com.entities.Product;
import ptit.com.model.PageInfo;
import ptit.com.service.ProductService;

@RestController
@RequestMapping("admin/product")
public class ProductRestController {
	@Autowired 
	public ProductService productService;
	
	@PostMapping("view")
	public Page<Product> getProductByPageInfo(@ModelAttribute("pageInfo") PageInfo pageInfo) {
		return productService.findByPageInfo(pageInfo);
	}
	
}
