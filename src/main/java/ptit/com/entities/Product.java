package ptit.com.entities;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString.Exclude;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Product extends BaseEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;	
	
	@NotBlank(message = "This field can not be empty!!!")
	private String name;
	
	@ManyToOne()
	@JoinColumn(name = "portfolio_id")
	@NotNull(message = "This field can not be null!!!")
	@Exclude
	private Portfolio portfolio;
	
	@Transient
	//@NotBlank(message = "This field can not be empty!!!")
	private Long portfolioId;
	
	@ManyToOne
	@JoinColumn(name = "category_id")
	@NotNull(message = "This field can not be null!!!")
	@Exclude
	private Category category;
	
	@Transient
	//@NotBlank(message = "This field can not be empty!!!")
	private Long categoryId;
	
	@ManyToOne
	@JoinColumn(name = "supplier_id")
	@NotNull(message = "This field can not be null!!!")
	@Exclude
	private Supplier supplier;
	
	@Transient
	//@NotBlank(message = "This field can not be empty!!!")
	private Long supplierId;
	
	@Transient
	//@NotBlank(message = "This field can not be empty!!!")
	private  MultipartFile imgFile1; 
	private String imgUrl1;
	
	
	@Transient
	//@NotBlank(message = "This field can not be empty!!!")
	private  MultipartFile imgFile2; 
	private String imgUrl2;
	
	@Transient
	//@NotBlank(message = "This field can not be empty!!!")
	private  MultipartFile imgFile3; 
	private String imgUrl3;
	
	@NotBlank(message = "This field can not be empty!!!")
	private String label;
	
	@NotNull(message = "This field can not be null!!!")
	private BigDecimal price;
	
	@NotNull(message = "This field can not be null!!!")
	private Long quantity;
	
	@NotBlank(message = "This field can not be empty!!!")
	private String keyword;
	
	private String description;
	
	
	
		
	
	
	
	
	
}
