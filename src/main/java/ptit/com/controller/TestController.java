package ptit.com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import ptit.com.model.PageInfo;
import ptit.com.repository.SupplierRepository;
import ptit.com.service.SupplierService;

@Controller
public class TestController {
	@Autowired
	SupplierService supplierService;
	
	@Autowired
	SupplierRepository supplierRepository;
	
	@GetMapping("/")
	public String hello() {
		return "admin/index";
	}
	
	
	
	
}
