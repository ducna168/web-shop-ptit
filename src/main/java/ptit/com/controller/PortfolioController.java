package ptit.com.controller;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import ptit.com.entities.Portfolio;
import ptit.com.model.PageInfo;
import ptit.com.service.PortfolioService;

@Controller
@RequestMapping("/admin/portfolio")
public class PortfolioController {
	
	@Autowired PortfolioService portfolioService;
	
	@GetMapping("/view")
	public String showAll(Model model) {
		model.addAttribute("pageInfo", new PageInfo());
		return "portfolio/view_portfolios";
	}
	
	@GetMapping("/add")
	public String showAddForm(Model model) {
		
		model.addAttribute("portfolio", new Portfolio());
		return "portfolio/add_portfolio";
		
	}
	
	@PostMapping("/add")
	public String addPortfolio(Model model,@Valid @ModelAttribute("portfolio") Portfolio portfolio,Errors errors) {
		System.out.println("Kkk");
		
		if (null != errors && errors.getErrorCount() > 0) {
			return "portfolio/add_portfolio";
		}
		else {
			String portfolioName = portfolio.getName().trim();
			if( portfolioService.existsByName(portfolioName)) {
				model.addAttribute("msg", portfolioName +" has already existed!!");
				return "portfolio/add_portfolio";
			}
			
			portfolioService.save(portfolio);
			return "redirect:/admin/portfolio/view";
		}
		
		
	}
	
	@GetMapping("/delete/{id}")
	public String deletePortfolio(@PathVariable("id") Long id) {
		portfolioService.deleteById(id);
		
		return "redirect:/admin/portfolio/view";
	}
	
	@GetMapping("/edit/{id}")
	public String showEditPortfolio(@PathVariable("id") Long id, Model model) {
		Portfolio portfolio = portfolioService.findById(id);
		model.addAttribute("portfolio", portfolio);
		return "portfolio/edit_portfolio";
	}
	
	@PostMapping("/edit")
	public String editPortfolio(Model model,@Valid @ModelAttribute("portfolio") Portfolio portfolio,Errors errors) {
	
		if (null != errors && errors.getErrorCount() > 0) {
			return "portfolio/add_portfolio";
		}
		else {
			portfolioService.save(portfolio);
			return "redirect:/admin/portfolio/view";
		}
	}
}
