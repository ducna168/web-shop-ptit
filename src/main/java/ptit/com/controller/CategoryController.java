package ptit.com.controller;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import ptit.com.entities.Category;
import ptit.com.model.PageInfo;
import ptit.com.service.CategoryService;

@Controller
@RequestMapping("/admin/category")
public class CategoryController {
	
	@Autowired CategoryService categoryService;
	
	@GetMapping("/view")
	public String showAll(Model model) {
		model.addAttribute("pageInfo", new PageInfo());
		return "category/view_categorys";
	}
	
	@GetMapping("/add")
	public String showAddForm(Model model) {
		
		model.addAttribute("category", new Category());
		return "category/add_category";
		
	}
	
	@PostMapping("/add")
	public String addCategory(Model model,@Valid @ModelAttribute("category") Category category,Errors errors) {
	
		if (null != errors && errors.getErrorCount() > 0) {
			return "category/add_category";
		}
		else {
			String categoryName = category.getName().trim();
			if( categoryService.existsByName(categoryName)) {
				model.addAttribute("msg", categoryName +" has already existed!!");
				return "category/add_category";
			}
			
			categoryService.save(category);
			return "redirect:/admin/category/view";
		}
		
		
	}
	
	@GetMapping("/delete/{id}")
	public String deleteCategory(@PathVariable("id") Long id) {
		categoryService.deleteById(id);
		
		return "redirect:/admin/category/view";
	}
	
	@GetMapping("/edit/{id}")
	public String showEditCategory(@PathVariable("id") Long id, Model model) {
		Category category = categoryService.findById(id);
		model.addAttribute("category", category);
		return "category/edit_category";
	}
	
	@PostMapping("/edit")
	public String editCategory(Model model,@Valid @ModelAttribute("category") Category category,Errors errors) {
	
		if (null != errors && errors.getErrorCount() > 0) {
			return "category/add_category";
		}
		else {
			categoryService.save(category);
			return "redirect:/admin/category/view";
		}
	}
}
