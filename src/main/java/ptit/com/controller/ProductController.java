package ptit.com.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import ptit.com.entities.Category;
import ptit.com.entities.Portfolio;
import ptit.com.entities.Product;
import ptit.com.entities.Supplier;
import ptit.com.model.PageInfo;
import ptit.com.repository.CategoryRepository;
import ptit.com.repository.PortfolioRepository;
import ptit.com.repository.ProductRepository;
import ptit.com.repository.SupplierRepository;
import ptit.com.utils.FileUploadUtil;

@Controller
@RequestMapping("/admin/product")
public class ProductController {
	
	@Autowired
	CategoryRepository categoryRepository;
	
	@Autowired
	PortfolioRepository portfolioRepository;
	
	@Autowired
	SupplierRepository supplierRepository;
	
	@Autowired
	ProductRepository productRepository;
	
	@GetMapping("/add")
	public String showAddForm(Model model) {
		model.addAttribute("product", new Product());
		
		//get list category
		List<Category> listCategory = categoryRepository.findAll();
		model.addAttribute("listCategory", listCategory);
		
		//get list portfolio
		List<Portfolio> listPortfolio = portfolioRepository.findAll();
		model.addAttribute("listPortfolio", listPortfolio);
		
		//get list supplier
		List<Supplier> listSupplier = supplierRepository.findAll();
		model.addAttribute("listSupplier", listSupplier);
		
		return "product/add_product";
	}
	
	@PostMapping("/add")
	public String add(@ModelAttribute("product") Product product) {
		Long categoryId = product.getCategoryId();
		Long profolioId = product.getPortfolioId();
		Long supplierId = product.getSupplierId();
		
		System.out.println(categoryId+"  "+profolioId+"  "+supplierId);
		
		// get list
		Optional<Category> categoryOpt = categoryRepository.findById(categoryId);
		Optional<Supplier> supplierOpt = supplierRepository.findById(supplierId);
		Optional<Portfolio> portfolioOpt = portfolioRepository.findById(profolioId);
		
		Category category = categoryOpt.get();
		Supplier supplier = supplierOpt.get();
		Portfolio portfolio = portfolioOpt.get();
		
		product.setCategory(category);
		product.setSupplier(supplier);
		product.setPortfolio(portfolio);
		
		
		//handle image
		MultipartFile image1 = product.getImgFile1();
		
		String imageName1 = StringUtils.cleanPath(image1.getOriginalFilename());
		
		MultipartFile image2 = product.getImgFile2();
		String imageName2 = StringUtils.cleanPath(image2.getOriginalFilename());
		
		MultipartFile image3 = product.getImgFile3();
		String imageName3 = StringUtils.cleanPath(image3.getOriginalFilename());
		
		String uploadDir = "images/product"+"/"+product.getName();
		
		String imgUrl1 = "/" +uploadDir +"/"+imageName1;
		String imgUrl2 = "/" +uploadDir +"/"+imageName2;
		String imgUrl3 = "/" +uploadDir +"/"+imageName3;
		
		System.out.println(imgUrl1);
		product.setImgUrl1(imgUrl1);
		System.out.println(imgUrl2);
		product.setImgUrl2(imgUrl2);
		System.out.println(imgUrl3);
		product.setImgUrl3(imgUrl3);
		
		//save to File
		try {
			FileUploadUtil.saveFile(uploadDir, imageName1, image1);
			FileUploadUtil.saveFile(uploadDir, imageName2, image2);
			FileUploadUtil.saveFile(uploadDir, imageName3, image3);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(product);
		productRepository.save(product);
		
		return "redirect:/admin/product/view";
	}
	
	@GetMapping("/view")
	public String showAll(Model model) {
		model.addAttribute("pageInfo", new PageInfo());
		return "product/view_products";
	}
	
}
