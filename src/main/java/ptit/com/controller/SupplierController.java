package ptit.com.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.Getter;
import ptit.com.entities.Supplier;
import ptit.com.model.PageInfo;
import ptit.com.service.SupplierService;

@Controller
@RequestMapping("/admin/supplier")
public class SupplierController {
	
	@Autowired SupplierService supplierService;
	
	@GetMapping("/view")
	public String showAll(Model model) {
		model.addAttribute("pageInfo", new PageInfo());
		return "supplier/view_suppliers";
	}
	
	@GetMapping("/add")
	public String showAddForm(Model model) {
		
		model.addAttribute("supplier", new Supplier());
		return "supplier/add_supplier";
		
	}
	
	@PostMapping("/add")
	public String addSupplier(Model model,@Valid @ModelAttribute("supplier") Supplier supplier,Errors errors) {
	
		if (null != errors && errors.getErrorCount() > 0) {
			return "supplier/add_supplier";
		}
		else {
			String supplierName = supplier.getName().trim();
			if( supplierService.existsByName(supplierName)) {
				model.addAttribute("msg", supplierName +" has already existed!!");
				return "supplier/add_supplier";
			}
			
			supplierService.save(supplier);
			return "redirect:/admin/supplier/view";
		}
		
		
	}
	
	@GetMapping("/delete/{id}")
	public String deleteSupplier(@PathVariable("id") Long id) {
		supplierService.deleteById(id);
		
		return "redirect:/admin/supplier/view";
	}
	
	@GetMapping("/edit/{id}")
	public String showEditSupplier(@PathVariable("id") Long id, Model model) {
		Supplier supplier = supplierService.findById(id);
		model.addAttribute("supplier", supplier);
		return "supplier/edit_supplier";
	}
	
	@PostMapping("/edit")
	public String editSupplier(Model model,@Valid @ModelAttribute("supplier") Supplier supplier,Errors errors) {
	
		if (null != errors && errors.getErrorCount() > 0) {
			return "supplier/add_supplier";
		}
		else {
			supplierService.save(supplier);
			return "redirect:/admin/supplier/view";
		}
	}
}
