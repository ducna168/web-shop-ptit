package ptit.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopwebptitApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShopwebptitApplication.class, args);
	}

}
