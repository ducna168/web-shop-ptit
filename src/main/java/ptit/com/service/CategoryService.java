package ptit.com.service;

import java.util.List;

import org.springframework.data.domain.Page;

import ptit.com.entities.Category;
import ptit.com.model.PageInfo;

public interface CategoryService {
	public Category save(Category category);
	public Boolean existsByName(String name);
	public Page<Category> findByPageInfo(PageInfo pageInfo);
	public void deleteById( Long id);
	public Category findById(Long id);
	
	public List<Category> findAll();
}
