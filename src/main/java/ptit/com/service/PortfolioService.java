package ptit.com.service;

import java.util.List;

import org.springframework.data.domain.Page;

import ptit.com.entities.Portfolio;
import ptit.com.model.PageInfo;

public interface PortfolioService {
	public Portfolio save(Portfolio portfolio);
	public Boolean existsByName(String name);
	public Page<Portfolio> findByPageInfo(PageInfo pageInfo);
	public void deleteById( Long id);
	public Portfolio findById(Long id);
	public List<Portfolio> findAll();
}
