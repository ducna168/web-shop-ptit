package ptit.com.service;

import java.util.List;

import org.springframework.data.domain.Page;

import ptit.com.entities.Product;
import ptit.com.model.PageInfo;

public interface ProductService {
	public Product save(Product product);
	public Boolean existsByName(String name);
	public Page<Product> findByPageInfo(PageInfo pageInfo);
	public void deleteById( Long id);
	public Product findById(Long id);
	public List<Product> findAll();
}
