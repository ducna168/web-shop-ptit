package ptit.com.service;

import java.util.List;

import org.springframework.data.domain.Page;

import ptit.com.entities.Supplier;
import ptit.com.model.PageInfo;

public interface SupplierService {
	public Supplier save(Supplier supplier);
	public Boolean existsByName(String name);
	public Page<Supplier> findByPageInfo(PageInfo pageInfo);
	public void deleteById( Long id);
	public Supplier findById(Long id);
	public List<Supplier> findAll();
}
