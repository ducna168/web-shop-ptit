package ptit.com.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ptit.com.constant.Constant;
import ptit.com.entities.Supplier;
import ptit.com.model.PageInfo;
import ptit.com.repository.SupplierRepository;
import ptit.com.service.SupplierService;

@Service
public class SupplierServiceImpl implements SupplierService {
	@Autowired
	SupplierRepository supplierRepository;

	@Override
	public Supplier save(Supplier supplier) {
		supplier.setIsActive(true);
		return supplierRepository.save(supplier);
	}

	@Override
	public Boolean existsByName(String name) {
		return supplierRepository.existsByName(name);
	}

	@Override
	public Page<Supplier> findByPageInfo(PageInfo pageInfo) {
		String name = pageInfo.getKeyword().trim();
		Integer pageSize = pageInfo.getPageSize();
		Integer pageNumber = pageInfo.getPageNumber();

		Integer sortType = pageInfo.getSortType();
		PageRequest pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by("name").ascending());
		
		if (sortType == Constant.SortType.DESCENDING) {
			pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by("name").descending());
		}

		Page<Supplier> page = supplierRepository.findByNameContains(name, pageRequest);

		return page;
	}

	@Override
	public void deleteById(Long id) {
		supplierRepository.deleteById(id);
	}

	@Override
	public Supplier findById(Long id) {
		Optional<Supplier> supplier = supplierRepository.findById(id);
		if( supplier.isPresent()) {
			return supplier.get();
		}
		return new Supplier();
	}

	@Override
	public List<Supplier> findAll() {
		return supplierRepository.findAll();
	}

}
