package ptit.com.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ptit.com.constant.Constant;
import ptit.com.entities.Portfolio;
import ptit.com.model.PageInfo;
import ptit.com.repository.PortfolioRepository;
import ptit.com.service.PortfolioService;

@Service
public class PortfolioServiceImpl implements PortfolioService {
	@Autowired
	PortfolioRepository portfolioRepository;

	@Override
	public Portfolio save(Portfolio portfolio) {
		portfolio.setIsActive(true);
		return portfolioRepository.save(portfolio);
	}

	@Override
	public Boolean existsByName(String name) {
		return portfolioRepository.existsByName(name);
	}

	@Override
	public Page<Portfolio> findByPageInfo(PageInfo pageInfo) {
		String name = pageInfo.getKeyword().trim();
		Integer pageSize = pageInfo.getPageSize();
		Integer pageNumber = pageInfo.getPageNumber();

		Integer sortType = pageInfo.getSortType();
		PageRequest pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by("name").ascending());
		
		if (sortType == Constant.SortType.DESCENDING) {
			pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by("name").descending());
		}

		Page<Portfolio> page = portfolioRepository.findByNameContains(name, pageRequest);

		return page;
	}

	@Override
	public void deleteById(Long id) {
		portfolioRepository.deleteById(id);
	}

	@Override
	public Portfolio findById(Long id) {
		Optional<Portfolio> portfolio = portfolioRepository.findById(id);
		if( portfolio.isPresent()) {
			return portfolio.get();
		}
		return new Portfolio();
	}

	@Override
	public List<Portfolio> findAll() {
		return portfolioRepository.findAll();
	}

}
