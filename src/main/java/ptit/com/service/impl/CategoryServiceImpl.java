package ptit.com.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ptit.com.constant.Constant;
import ptit.com.entities.Category;
import ptit.com.model.PageInfo;
import ptit.com.repository.CategoryRepository;
import ptit.com.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {
	@Autowired
	CategoryRepository categoryRepository;

	@Override
	public Category save(Category category) {
		category.setIsActive(true);
		return categoryRepository.save(category);
	}

	@Override
	public Boolean existsByName(String name) {
		return categoryRepository.existsByName(name);
	}

	@Override
	public Page<Category> findByPageInfo(PageInfo pageInfo) {
		String name = pageInfo.getKeyword().trim();
		Integer pageSize = pageInfo.getPageSize();
		Integer pageNumber = pageInfo.getPageNumber();

		Integer sortType = pageInfo.getSortType();
		PageRequest pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by("name").ascending());
		
		if (sortType == Constant.SortType.DESCENDING) {
			pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by("name").descending());
		}

		Page<Category> page = categoryRepository.findByNameContains(name, pageRequest);

		return page;
	}

	@Override
	public void deleteById(Long id) {
		categoryRepository.deleteById(id);
	}

	@Override
	public Category findById(Long id) {
		Optional<Category> category = categoryRepository.findById(id);
		if( category.isPresent()) {
			return category.get();
		}
		return new Category();
	}

	@Override
	public List<Category> findAll() {
		return categoryRepository.findAll();
	}
	
	

}
