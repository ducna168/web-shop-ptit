package ptit.com.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ptit.com.constant.Constant;
import ptit.com.entities.Product;
import ptit.com.model.PageInfo;
import ptit.com.repository.ProductRepository;
import ptit.com.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	ProductRepository productRepository;

	@Override
	public Product save(Product product) {
		product.setIsActive(true);
		return productRepository.save(product);
	}

	@Override
	public Boolean existsByName(String name) {
		return productRepository.existsByName(name);
	}

	@Override
	public Page<Product> findByPageInfo(PageInfo pageInfo) {
		String name = pageInfo.getKeyword().trim();
		Integer pageSize = pageInfo.getPageSize();
		Integer pageNumber = pageInfo.getPageNumber();

		Integer sortType = pageInfo.getSortType();
		PageRequest pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by("name").ascending());
		
		if (sortType == Constant.SortType.DESCENDING) {
			pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by("name").descending());
		}

		Page<Product> page = productRepository.findByNameContains(name, pageRequest);

		return page;
	}

	@Override
	public void deleteById(Long id) {
		productRepository.deleteById(id);
	}

	@Override
	public Product findById(Long id) {
		Optional<Product> product = productRepository.findById(id);
		if( product.isPresent()) {
			return product.get();
		}
		return new Product();
	}

	@Override
	public List<Product> findAll() {
		return productRepository.findAll();
	}
	
	

}
